package com.tc.itfarm.web.vo;

import com.tc.itfarm.model.User;

import java.util.List;

/**
 * Created by Administrator on 2016/9/5.
 */
public class UserVO extends User {
    private List<Integer> favoriteIds;

    private String roles;

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public List<Integer> getFavoriteIds() {
        return favoriteIds;
    }

    public void setFavoriteIds(List<Integer> favoriteIds) {
        this.favoriteIds = favoriteIds;
    }
}
