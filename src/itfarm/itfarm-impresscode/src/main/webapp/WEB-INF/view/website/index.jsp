<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title> ${config.webTitle} </title>
    <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="main_content" class="container">

    <div class="col-md-8">
        开发中....
    </div>
    <jsp:include page="../index/right.jsp"></jsp:include>
</div>
<%--底部--%>
<jsp:include page="../index/footer.jsp"/>
</body>
</html>