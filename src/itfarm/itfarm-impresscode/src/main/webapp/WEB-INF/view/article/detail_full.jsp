<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<%@ include file="/WEB-INF/layouts/base_style.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> ${config.webTitle} </title>
  <meta charset="utf-8">
</head>
<body>
<jsp:include page="../index/header.jsp"/>
<div id="nav-shadow"></div>
<div id="content">

  <div class="container">

    <div id="main" class="fullwidth">

      <div class="entry single">

        <div class="entry-header">

          <h2 class="title">${item.article.title }&nbsp;<a href="${ctx }/article/456${item.article.recordId}f.html" style="font-size: 10px;color: #8BBF5D;">(返回窄屏)</a></h2>

          <p class="meta">Published on <a href="#">${item.lastDate }</a> by <a href="#">${item.authorName }</a></p>

        </div><!-- end .entry-header -->

        <div class="entry-content">
          <%--广告图片--%>
          <div class="zoom align-right">
            <a class="single_image" href="img/sample-images/800x600.jpg">
              <img src="img/sample-images/650x210.jpg" width="210" height="210" alt="Texas Trip 2010: Abandoned ranch" class="entry-image" />
            </a>
          </div>

          <div class="zoom align-left">
            <a class="single_image" href="img/sample-images/800x600.jpg">
              <img src="img/sample-images/650x210.jpg" width="210" height="210" alt="Texas Trip 2010: Abandoned ranch" class="entry-image" />
            </a>
          </div>

          <p>${item.article.content }</p>

        </div><!-- end .entry-content -->

        <div class="entry-footer">

          <strong class="align-left">操 作: &nbsp;</strong>

          <dl class="horizontal">
            <dt>上一篇:</dt>
            <dd><a href="${ctx }/article/456id=${item.last.recordId}t.html">${item.last.title }</a> <span class="separator">|</span></dd>
            <dt>下一篇:</dt>
            <dd><a href="${ctx }/article/456${item.next.recordId}t.html">${item.next.title }</a> <span class="separator">|</span></dd>
            <dt><a href="${ctx }/article/456${item.article.recordId}f.html">评论</a></dt>
          </dl>

        </div><!-- end .entry-footer -->

      </div><!-- end .entry -->

    </div><!-- end #main -->

  </div><!-- end .container -->

</div><!-- end #content -->
<jsp:include page="../index/right.jsp"></jsp:include>
<%--底部--%>
<jsp:include page="../index/footer.jsp"/>

</body>
</html>