package com.tc.itfarm.api.exception;

import org.slf4j.Logger;

/**
 * Created by wangdongdong on 2016/8/12.
 */
public class BusinessException extends Exception {

    public BusinessException() {
        super("itfarm业务异常");
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 创建异常
     * @param logger
     * @param message
     * @return
     */
    public static BusinessException create(Logger logger, String message) {
        logger.error(message);
        return new BusinessException(message);
    }

    /**
     * 创建异常
     * @param logger
     * @param message
     * @param cause
     * @return
     */
    public static BusinessException create(Logger logger, String message, Throwable cause){
        logger.error(message);
        return new BusinessException(message, cause);
    }

    /**
     * 判断是否为true
     * @param expression
     * @param logger
     * @param message
     * @throws Exception
     */
    public static void checkTrue(boolean expression, Logger logger, String message) throws Exception {
        if (expression) {
            throw create(logger,message);
        }
    }
}
