package com.tc.itfarm.service.impl;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.dao.UserRoleDao;
import com.tc.itfarm.model.UserRole;
import com.tc.itfarm.model.UserRoleCriteria;
import com.tc.itfarm.service.UserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRole> implements UserRoleService {

	@Resource
	private UserRoleDao userRoleDao;

	@Override
	public List<UserRole> selectByUserId(Integer userId) {
		Assert.isTrue(userId != null, "userId不能为空");
		UserRoleCriteria criteria = new UserRoleCriteria();
		criteria.or().andUserIdEqualTo(userId);
		return userRoleDao.selectByCriteria(criteria);
	}

	@Override
	public Integer deleteByUserId(Integer userId) {
		UserRoleCriteria criteria = new UserRoleCriteria();
		criteria.or().andUserIdEqualTo(userId);
		return userRoleDao.deleteByCriteria(criteria);
	}

	@Override
	public Integer[] selectIdsByUser(Integer userId) {
		List<UserRole> userRoles = this.selectByUserId(userId);
		Integer [] ids = new Integer[userRoles.size()];
		int i = 0;
		for (UserRole ur : userRoles) {
			Integer roleId = ur.getRoleId();
			ids[i++] = roleId;
		}
		return ids;
	}

	@Override
	protected SingleTableDao getSingleDao() {
		return userRoleDao;
	}
}
