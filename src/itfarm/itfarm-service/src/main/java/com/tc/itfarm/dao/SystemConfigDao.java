package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.SystemConfig;
import com.tc.itfarm.model.SystemConfigCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SystemConfigDao extends SingleTableDao<SystemConfig, SystemConfigCriteria> {
}